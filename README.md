# coffee.sh
_Automating the important things in life_

Inspired by [this blog post](https://www.jitbit.com/alexblog/249-now-thats-what-i-call-a-hacker), these scripts are written to tailor to my specific needs. As a university student, that mostly means finding discounts/free stuff.


## License
These scripts are completely public domain; some may be slightly esoteric, but I hope that others can still find some use out of them. There is no need to give me credit if you use or modify any of these.
