# discount_drinks

_When you are going to have a long night ahead of you, you know exactly where to go._

I'm a university student. Finding cheap stuff is one of my most important goals. I just recently found out that our local convenience store, On the Run, sells any size drink for 50 cents after the St. Louis Cardinals score 6 or more runs in a game. However, that's annoying to have to check every day; why not automate it?

This script is intended to run inside a `cron` job every morning. Each day it will fetch the scores from the previous day. It will check whether the Cardinals played and whether their number of runs is greater than or equal to 6. If they did, it will notify the user through email, and profit!

This script, like all other scripts in this repository, is licensed as public domain. I have written it so that it can be easily modifiable and adaptable to different things; for example, if the Cubs score 5 or more runs, you simply need to modify `team_name` and `score_threshold` to match. It's not limited to just free drinks! Also make sure you set `from_email` and `to_email` correctly!

## Dependencies
`requests = 2.18`
