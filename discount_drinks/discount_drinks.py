#!/usr/bin/python3

from datetime import date
from datetime import timedelta
from email.mime.text import MIMEText

import requests
import smtplib

def fetch_scoreboard(d):
    r = requests.get(
        'http://gd2.mlb.com/components/game/mlb/year_{}/month_{:02d}/day_{:02d}/miniscoreboard.json' \
        .format(d.year, d.month, d.day))
    if r.status_code == 200:
        return r.json()
    return None


def count_runs(scores, team):
    for game in scores['data']['games']['game']:
        if game['home_team_name'] == team:
            return int(game['home_team_runs'])
        elif game['away_team_name'] == team:
            return int(game['away_team_runs'])

    return -1 # when no matching results are found


if __name__ == '__main__':

    # Runtime configuration
    team_name = 'Cardinals'
    score_threshold = 6
    to_email = 'to@example.com'
    from_email = 'from@example.com'

    d = date.today() - timedelta(days=1); # Yesterday
    runs = count_runs(fetch_scoreboard(d), team_name)

    if runs >= score_threshold:
        print('Discount detected!')

        msg = MIMEText('Make sure to stop by On the Run to get your soda.')
        msg['Subject'] = '50 cent sodas are today!!!!'
        msg['From'] = from_email
        msg['To'] = to_email

        s = smtplib.SMTP('localhost')
        s.sendmail(from_email, [to_email], msg.as_string())
        s.quit()
